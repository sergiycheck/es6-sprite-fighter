import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  
  // todo: show fighter info (image, name, health, etc.)
  
  //const {name, health, attack, defense} = fighter; //getting properties 
  if(fighter!=undefined){
    const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
    fighterName.innerHTML = fighter.name;

    const fighterImg = createFighterImage(fighter);
    fighterElement.append(fighterName);

    let mapFighter = new Map(Object.entries(fighter));
    mapFighter.forEach((key,value)=>{
      if(value!="name"&&value!="_id"&&value!="source"){
        const fighterInfo = createElement({tagName:'span',className:'arena___fighter-properties'});
        fighterInfo.innerHTML = `${key} : ${value}`;
        fighterElement.append(fighterInfo);
      }
      
    });
    fighterElement.append(fighterImg);
    return fighterElement;
  }


}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
