import { controls } from '../../constants/controls';
import {createElement } from '../helpers/domHelper';



let fightersCriticalHitTimer={
  canFirstStartCriticalHit:false,
  canSecondStartCriticalHit:false
}

let winnerOfFight={
  fighterWinner:undefined,
  first:undefined,
  second:undefined
}

export function createTimerElement(timerId,position){
  const p1 = createElement({
    tagName:"p",
    className:"arena___fighter-name",
    attributes:{
      id:timerId
    }
  });
  p1.style.webkitTextFillColor  = "#43FC73";
  document.querySelectorAll('.arena___fighter-indicator')[position].append(p1);
}
export function createFightTimerElement(timerId){
  const timerEl = createElement({
    tagName:"span",
    className:"arena_timer",
    attributes:{
      id:timerId
    }
  });
  document.querySelector('.arena___versus-sign').append(timerEl);
}



function handleCriticalHit(fighter,firstFighter,secondFighter){

  let winner;
  if(fighter=="first"){
    if(fightersCriticalHitTimer.canFirstStartCriticalHit){

      console.log("player one scores a critical hit");
      winner = handleAttack(firstFighter,secondFighter,"right",getDamageOfCriticalHit);
      setCriticalHitTimer(fightersCriticalHitTimer,true,false);
  
    }
  }else if(fighter=="second"){
    if(fightersCriticalHitTimer.canSecondStartCriticalHit){
      console.log("player two scores a critical hit");
      winner = handleAttack(firstFighter,secondFighter,"left",getDamageOfCriticalHit);
      setCriticalHitTimer(fightersCriticalHitTimer,false,true);
    }
  }

  return winner;

}



function handleFightersKeys(
  firstFighter,
  secondFighter,
  keys,
  canFirstBeHit,
  canSecondBeHit
  ){

  //todo add critical hit handling

  let winnerReturned;

  if(Object.keys(keys).length==3){

    if(keys[controls.PlayerOneCriticalHitCombination[0]]&&
       keys[controls.PlayerOneCriticalHitCombination[1]]&&
       keys[controls.PlayerOneCriticalHitCombination[2]]){
        winnerReturned = handleCriticalHit("first",firstFighter,secondFighter);
    }else if(
      keys[controls.PlayerTwoCriticalHitCombination[0]]&&
      keys[controls.PlayerTwoCriticalHitCombination[1]]&&
      keys[controls.PlayerTwoCriticalHitCombination[2]]){
        winnerReturned = handleCriticalHit("second",secondFighter,firstFighter);
      }
  }



  //arena___fighter all fighters class
  //arena___right-fighter
  //arena___left-fighter

  //health bar  className:arena___health-bar, ids:
  //left-fighter-indicator
  //right-fighter-indicator

  //todo add animation 
  
  if(keys[controls.PlayerOneAttack]){
    console.log("player one attacks");
    if(canSecondBeHit && canFirstBeHit){
      winnerReturned = handleAttack(firstFighter,secondFighter,"right",getDamage);
    }
  }else if(keys[controls.PlayerOneBlock]){
    if(canFirstBeHit){
      canFirstBeHit = false;
      console.log(`down canFirstBeHit ${canFirstBeHit}`);
    }
  }else if(keys[controls.PlayerTwoAttack]){
    console.log("player two attacks");
    if(canSecondBeHit && canFirstBeHit){
      winnerReturned = handleAttack(secondFighter,firstFighter,"left",getDamage);
    }
  }else if(keys[controls.PlayerTwoBlock]){
    if(canSecondBeHit){
      canSecondBeHit = false;
      console.log(`down canSecondBeHit ${canSecondBeHit}`);
    }
  }
  let winner;
  if(winnerReturned!==undefined){
    winner = winnerReturned;
    winnerOfFight.fighterWinner = winner;
    console.log(`WINNER`);
    console.log(winner);
    console.log(winnerOfFight);
  }

  return {
    boolFirstHit:canFirstBeHit,
    boolSecondHit:canSecondBeHit};
}

function handleAttack(firstFighter,secondFighter,position,damageHandler){
  console.log(`${position} fighter health before hit ${secondFighter.health}`);
  let damage = damageHandler(firstFighter,secondFighter);
  let percentageDamage = Math.floor((damage/secondFighter.health)*100);
  console.log(`damage ${damage} percentageDamage ${percentageDamage}`);
  if(secondFighter.health>0){
    secondFighter.health-=damage;
  }else{
    return firstFighter;
  }
  
  let positionHealthBar = document.getElementById(`${position}-fighter-indicator`);
  let currentBarWidth = 0;
  let styleWidth = positionHealthBar.style.width;
  if(styleWidth==""||styleWidth==undefined){
    currentBarWidth = 100;
  }else{
    let wRegx = /\d+/;
    let widthNum = wRegx.exec(styleWidth);
    currentBarWidth = Math.floor(widthNum-percentageDamage);
  }
  positionHealthBar.style.width =`${currentBarWidth}%`;

  console.log(`${position} fighter health after hit ${secondFighter.health}`);

}


export function setListenersForFight(firstFighter, secondFighter){

  let keys = [];
  let canFirstBeHit = true;
  let canSecondBeHit = true;

  setCriticalHitTimer(fightersCriticalHitTimer,true,true);

  window.addEventListener('keydown',(event)=>{
    if (event.defaultPrevented) {
      return; 
    }
    keys[event.code] = true;
    let {
      boolFirstHit,
      boolSecondHit}=handleFightersKeys(
                              firstFighter,
                              secondFighter,
                              keys,
                              canFirstBeHit,
                              canSecondBeHit);
      
      canFirstBeHit = boolFirstHit;
      canSecondBeHit = boolSecondHit;

      console.log(`returned bool can hit values ${canFirstBeHit} ${canSecondBeHit}`);
    event.preventDefault();
  },true);
  
  window.addEventListener("keyup",(e)=>{
    if(e.code==controls.PlayerOneBlock){
      canFirstBeHit = true;
      console.log(`up canFirstBeHit ${canFirstBeHit}`);
    }else if(e.code==controls.PlayerTwoBlock){
      canSecondBeHit = true;
      console.log(`up canSecondBeHit ${canSecondBeHit}`);
    }
    keys[e.code] = false;
    if(!canFirstBeHit){
      keys = keys.filter(elem=>elem!==controls.PlayerOneBlock);
    } else if(!canSecondBeHit){
      keys = keys.filter(elem=>elem!==controls.PlayerTwoBlock);
    }else{
      keys =keys.filter(elem=>elem!==e.code);
    }
    
    
  },true);
}

function setCriticalHitTimer(timer,first,second){
  let criticalHitTime = 10000;
  if(first){
    timer.canFirstStartCriticalHit = false;
    console.log(`canFirstStartCriticalHit ${timer.canFirstStartCriticalHit}`);
    timerFire("firstCriticalTimer","critical hit time",criticalHitTime,"ready for critical hit");
    setTimeout(()=>{
      timer.canFirstStartCriticalHit  = true;
      console.log(`canFirstStartCriticalHit ${timer.canFirstStartCriticalHit}`);
    },criticalHitTime);
  }
  if (second){
    timer.canSecondStartCriticalHit = false;
    console.log(`canSecondStartCriticalHit ${timer.canSecondStartCriticalHit}`);
    timerFire("secondCriticalTimer","critical hit time",criticalHitTime,"ready for critical hit");
    setTimeout(()=>{
      timer.canSecondStartCriticalHit = true;
      console.log(`canSecondStartCriticalHit ${timer.canSecondStartCriticalHit}`);
    },criticalHitTime);
  }

}

export async function timerFire(timerId,timerText,time,expiredWord){
  
  const el = document.getElementById(timerId);
  let timeMiliseconds = time;
  const timeInterval = setInterval(()=>{
    let now = 1000;
    timeMiliseconds -= now;
    let minutes = Math.floor((timeMiliseconds % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((timeMiliseconds % (1000 * 60)) / 1000);
    el.innerHTML = `${timerText} ${minutes} m ${seconds} s`;
    if (timeMiliseconds < 0) {
      clearInterval(timeInterval);
      el.innerHTML = expiredWord;
    }
  },1000);

}


export async function fight(firstFighter, secondFighter) {
  const fightTime = 300000;
  winnerOfFight.first = firstFighter;
  winnerOfFight.second = secondFighter;
  createFightTimerElement("mainTimer");
  timerFire("mainTimer","",fightTime,"Fight finished");
  
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    let winner;
    setTimeout(()=>{
      winner = winnerOfFight.fighterWinner;
      if(winner==undefined){
        if(winnerOfFight.first.health>winnerOfFight.second.health){
          winner = winnerOfFight.first;
        }else if(winnerOfFight.second.health>winnerOfFight.first.health) {
          winner = winnerOfFight.second;
        }
      }
      resolve(winner);//todo set winner
    },fightTime)//time of fight 5 min

    let winnerInterval = setInterval(()=>{
      if(winnerOfFight.fighterWinner!==undefined){
        winner = winnerOfFight.fighterWinner;
        clearInterval(winnerInterval);
        resolve(winner);
      }
    },1000/24);


  });
}



export function getDamage(attacker, defender) {
  // return damage
  const blockPower = getBlockPower(defender);
  console.log(`blockPower ${blockPower}`);
  const hitPower = getHitPower(attacker);
  console.log(`hitPower ${blockPower}`);
  if(blockPower>hitPower)
  {
    console.log(`${attacker} cannot hit ${defender}`);
    return 0;
  }
  return hitPower-blockPower;
}

export function getDamageOfCriticalHit(attacker,defender){
  const criticalHitPower = getCriticalHitPower(attacker);
  const blockPower = getBlockPower(defender);
  console.log(`blockPower ${blockPower} but cannot be applied`);
  console.log(`criticalHitPower ${criticalHitPower}`);
  return criticalHitPower;
}

export function getCriticalHitPower(fighter){
  let criticalHitDamage = fighter.attack*2;
  console.log(`critical Hit ${fighter.name}, damage ${criticalHitDamage}`);
  return criticalHitDamage;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance  = getRandomNumber(1,2);
  console.log(`criticalHitChance ${criticalHitChance}`);
  return fighter.attack*criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = getRandomNumber(1,2);
  console.log(`dodgeChance ${dodgeChance}`);
  return fighter.defense*dodgeChance;
}

function getRandomNumber(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min; 
}

