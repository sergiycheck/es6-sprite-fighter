import {showModal  } from "./modal";
import {createFighterImage} from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function 
  const fighterImg = createFighterImage(fighter);
  showModal({
    title:"winner "+fighter.name,
    bodyElement:fighterImg,
    onClose:()=>{
      document.location.reload();
    }});

}
